# Raspberry PiHAT

Creating an uninterruptable power supply for a raspberry pi computer.

The microHat is a UPS (uninterrupted power supply). This microHat powers up a Raspberry Pi. This can be used in the event that the power source for the Pi is cut off due to load shedding, an electrical or mechanical fault that results in power cut randomly. This Hat will attach to the 40pin GPIO for Raspberry Pi on top.

Subsystem 1: Voltmeter and signal opamp constraining values to 0-3.3V
5 Rechargeable batteries each with a voltage output of 3.7V give out a total voltage that is fed into the voltmeter. The voltmeter determines how much the battery is charged. The voltmeter opamps each have a reference voltage that is compared to the battery voltage and if the battery voltage is higher than the reference voltage then the opamp will pull down the output to a constrained value between 0-3.3V. These Pi volts will be fed into Raspberry Pi. This subsystem links with the 3rd subsystem of status LEDs through the Raspberry Pi.

Subsystem 2: Power circuitry to convert battery voltage to correct Pi voltages
A voltage regulator is implemented to regulate voltage from either the power supply or the battery supply. Raspberry pi has Pi voltages and the voltage regulator outputs Pi voltages. The voltage regulator takes in a high voltage input and steps it down to a lower voltage suitable for pi. The voltage regulator has a DC-DC buck converter design.
On the input, a large electrolytic capacitor is in parallel with a ceramic capacitor, this ensures the LM2678 Regulator chip being used can switch to different current inputs at very high speeds. A Schottky diode is used as it has low forward voltages and produces low heat. More ceramic capacitors and an inductor help smooth out the high frequency of the switching waveform voltage leaving us with a smooth DC voltage to be output. Resistors at the end ensure we output a voltage that is required by the Raspberry Pi of 5V.

The subsystem interacts with Raspberry Pi by being connected to the GPIO.

Subsystem 3: Status LEDs
4 Status LEDs are connected to 4 GPIO pins of the Raspberry Pi. When different voltage values have been received a diode with a certain colour lights up to indicate how much power is left in the battery. This subsystem interacts with the user by being a warning signal for when to switch off your pi and prevent data from being corrupted. 
